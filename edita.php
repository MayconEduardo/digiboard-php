<?php
if (!empty($_POST)) {
    if ($id = (int)$_GET['id']) {
        $parametros = json_encode($_POST);
        $url = 'http://localhost:3000/pessoas/'.$id;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parametros);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($parametros))                                                                       
        );
        $pessoas = json_decode(curl_exec($ch));
        curl_close($ch);
    }
    header('Location: /digiboard-php');
}

if ($id = (int)$_GET['id']) {
    $url = 'http://localhost:3000/pessoas/'.$id;
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $pessoa = json_decode(curl_exec($ch));
    if ($pessoa) {
        $nb_matricula = $pessoa->nb_matricula;
        $tx_nome = $pessoa->tx_nome;
        $tx_cpf = $pessoa->tx_cpf;
    } else {
        header('Location: /digiboard-php');
    }
}
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <title>Digiboard</title>
</head>

<body>
    <div class="container">
        <div class="jumbotron">
            <h1>Edição de Pessoa</h1>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/digiboard-php">Lista de Pessoas</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edição de Pessoa</li>
            </ol>
        </nav>
        <form action="" method="post">
            <div class="form-group">
                <label for="nb_matricula">Matrícula</label>
                <input type="text" class="form-control" id="nb_matricula" name="nb_matricula" maxlength="10" onkeyup="somenteNumeros(this);" required value="<?= $nb_matricula ?>">
            </div>
            <div class="form-group">
                <label for="tx_nome">Nome</label>
                <input type="text" class="form-control" id="tx_nome" name="tx_nome" maxlength="50" required value="<?= $tx_nome ?>">
            </div>
            <div class="form-group">
                <label for="tx_cpf">CPF</label>
                <input type="text" class="form-control" id="tx_cpf" name="tx_cpf" maxlength="14" required value="<?= $tx_cpf ?>">
            </div>
            <button type="submit" class="btn btn-primary">Salvar</button>
        </form>
    </div>
    <script>
        function somenteNumeros(num) {
            var er = /[^0-9.]/;
            er.lastIndex = 0;
            var campo = num;
            if (er.test(campo.value)) {
            campo.value = "";
            }
        }
    </script>
</body>

</html>