<?php
$busca = '';
if ($_POST) {
    if ($_POST['buscar']) {
        $buscar = $_POST['buscar'];
        switch ($_POST['rdbBuscar']) {
            case '1':
                $busca = '/matricula/'.(int)$buscar;
                break;
    
            case '2':
                $busca = '/nome/'.$buscar;
                break;
    
            case '3':
                $busca = '/cpf/'.$buscar;
                break;
            
            default:
                $busca = '';
                break;
        }
    }
}
$url = 'http://localhost:3000/pessoas'.$busca;
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$pessoas = json_decode(curl_exec($ch));
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <title>Digiboard</title>
</head>

<body>
    <div class="container">
        <div class="jumbotron">
            <h1>Lista de Pessoas</h1>
        </div>
        <a href="cadastra.php" class="btn btn-primary mb-2">Nova Pessoa</a>

        <form action="" method="post" class="form-inline float-right">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="rdbBuscar" id="rdbMatricula" value="1" checked>
                <label class="form-check-label" for="rdbMatricula">Matrícula</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="rdbBuscar" id="rdbNome" value="2">
                <label class="form-check-label" for="rdbNome">Nome</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="rdbBuscar" id="rdbCPF" value="3">
                <label class="form-check-label" for="rdbCPF">CPF</label>
            </div>
            <div class="form-group mx-sm-3 mb-2">
                <input type="text" class="form-control" id="buscar" name="buscar" placeholder="Buscar">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Buscar</button>
        </form>

        <ul class="list-group">
            <?php if (empty($pessoas)) { ?>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <span class="text-center"></span>
                    <span class="text-center">Sem registros</span>
                    <span class="text-center"></span>
                </li>
            <?php } ?>
            <?php foreach ($pessoas as $pessoa): ?>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <span>
                    <img src="<?= ($pessoa->tx_foto) ? $pessoa->tx_foto : 'assets/img/sem-imagem-avatar.jpg' ?>" alt="..." class="img-thumbnail" style="width: 150px; height: 180px">
                </span>
                <span class="text-center">
                    <strong>Matrícula:</strong> <?= $pessoa->nb_matricula ?><br>
                    <strong>Nome:</strong> <?= $pessoa->tx_nome ?><br>
                    <strong>CPF:</strong> <?= $pessoa->tx_cpf ?>
                </span>
                <span>
                    <a href="edita.php?id=<?= $pessoa->id_pessoa ?>" class="btn btn-info">
                        Editar
                    </a>
                    <a href="exclui.php?id=<?= $pessoa->id_pessoa ?>" class="btn btn-danger" onclick="return confirm('Deseja excluir <?= $pessoa->tx_nome ?> ?');">
                        Excluir
                    </a>
                </span>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</body>

</html>