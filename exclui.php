<?php
if ($id = (int)$_GET['id']) {
    $url = 'http://localhost:3000/pessoas/'.$id;
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');                                                                     
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
    $pessoas = json_decode(curl_exec($ch));
    curl_close($ch);
}
header('Location: /digiboard-php');